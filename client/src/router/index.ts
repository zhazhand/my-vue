import Vue from "vue";
import VueRouter from "vue-router";
import PatientList from "../pages/PatientList.vue";
import PatientDetail from "../pages/PatientDetail.vue";
import PatientInfoPage from "../pages/PatientInfoPage.vue";
import ProcedureForm from "../pages/ProcedureForm.vue";

Vue.use(VueRouter);

const routes = [
	{
		path: "/",
		name: "home",
		component: PatientList
	},
	{
		path: "/patient",
		name: "patient",
		component: PatientList
	},
	{
		path: "/patient/:id",
		component: PatientDetail,
		props: true,
		children: [
			{ path: "", component: PatientInfoPage, name: "patientInfo" },
			{ path: "procedure", component: ProcedureForm, name: "newProcedure" }
		]
	}
];

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes
});

export default router;
