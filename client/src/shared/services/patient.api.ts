import Axios from "axios";
import { Patient } from "@/shared/interfaces/Patient";

/* export async function getAllPatients(): Promise<Patient[]> {
	const response = await Axios.get("http://localhost:3000/patients");
	console.log(response.data);
	console.log(response.status);
	console.log(response.statusText);
	return response.data;
} */
export default class PatientAPIService {
	public static async getAllPatients(): Promise<Patient[] | any[]> {
		const response = await Axios.get("http://localhost:3000/patients");
		return response.data;
	}
}
