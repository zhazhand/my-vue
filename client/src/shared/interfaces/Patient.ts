interface Location {
	address?: string;
	addressAdditional?: string;
	city: string;
	state: string;
	zip: number;
}

interface Procedure {
	name: string;
	date?: Date;
	amount?: number;
	provider: string;
	ensurance: boolean;
	status: string;
	detail?: string;
}

export interface Procedures {
	[index: number]: Procedure;
}

export interface Patient {
	id: string;
	firstName: string;
	secondName: string;
	email: string;
	phone: number;
	garantor: boolean;
	location: Location;
	procedures?: Procedures;
}
