import { Patient } from "@/shared/interfaces/Patient";

export interface Clients {
	clients: Patient[];
}

export default {
	state: {
		clients: []
	},
	mutations: {
		changeList(state: Clients, payload: any) {
			state.clients = payload;
		}
	},
	actions: {},
	getters: {
		getAllClients(state: Clients) {
			return state.clients;
		},
		getOneClient(state: Clients) {
			const allClients = state.clients;
			return (clientId: any) => {
				return allClients.find(item => item.id === clientId);
			};
		}
	}
};
